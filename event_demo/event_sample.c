/* 
 *
 * Test application. This application
 * will launch a kernel in PEAK. The kernel will just
 * compute a matrix multiplication. The size of the matrix 
 * and the matrices pointers will be passed as parameters
 *
 * After executing the kernel, the application shows
 * the resulting output matrix
 *
 * This application demonstrates read & write communication between
 * kernel and host application.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "host/mango.h"

#define KID 1
#define BID_1 1
#define BID_2 2

int main(int argc, char**argv) {

  /* initialization of the mango context */
  mango_init("event", "generic");

  kernelfunction *k_func = mango_kernelfunction_init();
#ifdef GNEMU
  char kernel_file[] = "/opt/mango/usr/local/share/event/event";
  mango_load_kernel(kernel_file, k_func, GN, BINARY);
#else
  char kernel_file[] = "/opt/mango/usr/local/share/event/memory.data.fpga.datafile";
  mango_load_kernel(kernel_file, k_func, PEAK, BINARY);
#endif


	int buffer_size = 5;
	int16_t buffer[buffer_size];

	mango_kernel_t k          = mango_register_kernel(KID, k_func, 0, 1, BID_1);  
	mango_buffer_t b_to_dev   = mango_register_memory(BID_1, buffer_size * sizeof(int16_t), BUFFER, 1, 0, k);  
	mango_buffer_t b_from_dev = mango_register_memory(BID_2, buffer_size * sizeof(int16_t), BUFFER, 0, 1, k);  
	mango_event_t  e_to_dev   = mango_get_buffer_event(b_to_dev);
	mango_event_t  e_from_dev = mango_get_buffer_event(b_from_dev);
 
	/* Registration of task graph */
	mango_task_graph_t *tg = mango_task_graph_create(1, 2, 0, k, b_to_dev, b_from_dev);

	/* resource allocation */
	mango_resource_allocation(tg);

	/* Execution preparation */
	mango_arg_t *arg1 = mango_arg(k, &b_to_dev, sizeof(uint64_t), BUFFER);
	mango_arg_t *arg2 = mango_arg(k, &b_from_dev, sizeof(uint64_t), BUFFER);
	mango_arg_t *arg3 = mango_arg(k, &e_to_dev, sizeof(uint64_t), EVENT);
	mango_arg_t *arg4 = mango_arg(k, &e_from_dev, sizeof(uint64_t), EVENT);
	mango_arg_t *arg5 = mango_arg(k, &buffer_size, sizeof(buffer_size), SCALAR);

	mango_args_t *args=mango_set_args(k, 5, arg1, arg2, arg3, arg4, arg5);

	mango_write_synchronization(e_to_dev, LOCK);
	mango_write_synchronization(e_from_dev, LOCK);

	/* spawn kernel */
	mango_event_t ev = mango_start_kernel(k, args, 0);


	for (int i=0; i < 10; i++) {
		printf("signal\te_from_dev\tWRITE\n");
		mango_write_synchronization(e_from_dev, WRITE);
		printf("wait\te_to_dev\tWRITE\n");
		mango_wait_state(e_to_dev, WRITE);
		for (int j=0; j < buffer_size; j++) {
			buffer[j] = j;
		}
		printf("Writing to buffer...\n");
		mango_write(buffer, b_to_dev, DIRECT, 0);
		printf("signal\te_to_dev\tREAD\n");
		mango_write_synchronization(e_to_dev, READ);

		printf("wait\te_to_dev\tREAD\n");
		mango_wait_state(e_from_dev, READ);
		printf("Reading from buffer...\n");
		mango_read(buffer, b_from_dev, DIRECT, 0);
		for (int j=0; j < buffer_size; j++) {
			printf("CHECK %d==%d\n", buffer[j], j*j);
			assert(buffer[j] == j*j);
		}
	}

	mango_wait(ev);

	/* shut down the mango infrastructure */
	mango_resource_deallocation(tg);
	mango_task_graph_destroy_all(tg);
	mango_release();

	return 0;	
}	


